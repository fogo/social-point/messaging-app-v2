package comms

import (
	"context"
	"errors"
)

var (
	// ErrDeviceNotFound is returned by the repository
	// when trying to find a non-existent device identifier.
	ErrDeviceNotFound = errors.New("device not found")

	// ErrDeviceNotRegistered is returned when trying to send/fetch
	// messages with a non-registered sender/receiver device identifier.
	ErrDeviceNotRegistered = errors.New("device not registered")

	// ErrDeviceIDAlreadyExists is returned by the repository
	// when trying to save a device with an already registered
	// device identifier.
	ErrDeviceIDAlreadyExists = errors.New("device id already exists")
)

// DeviceID is a device identifier.
type DeviceID string

// NewDeviceID returns a new DeviceID instance.
// Returns error if there are missing requirements.
func NewDeviceID(id string) (DeviceID, error) {
	return DeviceID(id), nil
}

// String implements the Stringer interface.
// Used to convert the given device identifier to a string.
func (id DeviceID) String() string {
	return string(id)
}

// Device is identified by a DeviceID
type Device struct {
	id       DeviceID
	clientIP string
}

func (d Device) ID() DeviceID {
	return d.id
}

func (d Device) ClientIP() string {
	return d.clientIP
}

// NewDevice returns a new Device instance.
// Returns error if there are missing requirements.
func NewDevice(id, clientIP string) (Device, error) {
	deviceID, err := NewDeviceID(id)
	if err != nil {
		return Device{}, err
	}

	return Device{id: deviceID, clientIP: clientIP}, nil
}

// DeviceRepository defines the expected behavior
// from the devices persistence layer abstraction.
type DeviceRepository interface {
	// Save stores the given device.
	Save(context.Context, Device) error
	// SearchByDeviceID looks for a device with the given identifier.
	SearchByDeviceID(context.Context, DeviceID) (Device, error)
}

// DeviceRegistrar defines the expected behavior
// from a component capable of registering devices.
type DeviceRegistrar interface {
	// Register sends the given message.
	Register(context.Context, DeviceID) error
}
