package version

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVersion(t *testing.T) {
	rescueStdout := os.Stdout
	r, w, _ := os.Pipe()
	os.Stdout = w

	testName := "Test"
	testVersion := "v0.0.0"
	expectedOutput := fmt.Sprintf("%s %v\n", testName, testVersion)

	versionCmd := NewVersionCmd(context.Background(), testName, testVersion)
	versionCmd.Run(versionCmd, nil)

	require.NoError(t, w.Close())
	got, _ := ioutil.ReadAll(r)
	os.Stdout = rescueStdout

	if string(got) != expectedOutput {
		t.Errorf("Expected %s, got %s", expectedOutput, got)
	}
}
