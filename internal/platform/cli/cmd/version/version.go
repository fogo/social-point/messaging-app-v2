package version

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
)

// NewVersionCmd returns a cobra.Command to query the application version.
func NewVersionCmd(_ context.Context, appName, appVersion string) *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Prints the version number",
		Long: `
Prints the version number of the running sotware.
`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("%s %v\n", appName, appVersion)
		},
	}
}
