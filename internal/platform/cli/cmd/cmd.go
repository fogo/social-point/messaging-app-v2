package cmd

import (
	"context"

	"github.com/spf13/cobra"
)

// NewClientCmd returns a cobra.Command to run the client.
func NewClientCmd(_ context.Context) *cobra.Command {
	return &cobra.Command{
		Use:   "primcom",
		Short: "primcom is the CLI tool to manage the Primitive Communications services",
		Long: `
Command-line interface designed to communicate with the 
primcom daemon (primcomd) used to manage the Primitive Communications services.

Register your device and start sending and receiving messages.
`,
		RunE: func(cmd *cobra.Command, args []string) error {
			return cmd.Help()
		},
		SilenceUsage:  true,
		SilenceErrors: true,
	}
}
