package send

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal"
)

// NewSendCmd returns a cobra.Command to send a new message.
func NewSendCmd(ctx context.Context, sender comms.MessageSender) *cobra.Command {
	return &cobra.Command{
		Use:   "send <from> <to> <message>",
		Short: "Sends a new message from / to the given device identifier",
		Long: `
Asks the primcom daemon (primcomd) to send 
a new message from / to the given device identifier.
`,
		Args: cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			message, err := comms.NewMessage(args[0], args[1], args[2])
			if err != nil {
				return err
			}

			err = sender.Send(ctx, message)
			if err != nil {
				return err
			}

			fmt.Println("Message sent successfully!")
			return nil
		},
	}
}
