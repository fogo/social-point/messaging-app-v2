package messages

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/creating"
)

func CreateMessage(creatingService creating.Service) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var req createMessageRequest
		if err := ctx.ShouldBindJSON(&req); err != nil {
			response := map[string]string{"message": err.Error()}
			ctx.JSON(http.StatusBadRequest, response)
			return
		}

		err := creatingService.CreateMessage(ctx.Request.Context(), req.From, req.To, req.Body)
		if err != nil {
			response := map[string]string{"message": err.Error()}

			if errors.Is(err, comms.ErrDeviceNotRegistered) {
				ctx.JSON(http.StatusUnauthorized, response)
				return
			}

			ctx.JSON(http.StatusInternalServerError, response)
			return
		}

		response := map[string]string{"message": "ok"}
		ctx.JSON(http.StatusAccepted, response)
	}
}
