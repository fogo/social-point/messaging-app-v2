package sender

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"gitlab.com/fogo/social-point/messaging-app-v2/internal"
)

// Sender is an HTTP implementation of the comms.MessageSender
// interface returned by http.NewSender().
type Sender struct {
	client   *http.Client
	endpoint *url.URL
}

// NewSender returns a new instance of an HTTP implementation
// of the comms.MessageSender interface.
func NewSender(client *http.Client, endpoint *url.URL) *Sender {
	return &Sender{
		endpoint: endpoint,
		client:   client,
	}
}

// Save implements the comms.MessageSender interface.
func (r *Sender) Send(ctx context.Context, msg comms.Message) error {
	// Build the request
	req, err := buildSendRequest(ctx, r.endpoint, msg)
	if err != nil {
		return fmt.Errorf("cannot send the message: %w", err)
	}

	// Perform request and assert it was a success
	err = r.performSendRequest(req)
	if err != nil {
		return fmt.Errorf("cannot send the message: %w", err)
	}

	return nil
}

func (r *Sender) performSendRequest(req *http.Request) error {
	resp, err := r.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusUnauthorized {
		return comms.ErrDeviceNotRegistered
	}

	if resp.StatusCode != http.StatusAccepted {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return fmt.Errorf("unexpected http response: %s", body)
	}

	return nil
}
