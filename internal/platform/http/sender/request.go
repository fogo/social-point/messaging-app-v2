package sender

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/url"

	"gitlab.com/fogo/social-point/messaging-app-v2/internal"
)

type sendRequest struct {
	From string `json:"from"`
	To   string `json:"to"`
	Body string `json:"body"`
}

func buildSendRequest(ctx context.Context, endpoint *url.URL, msg comms.Message) (*http.Request, error) {
	reqBytes, err := json.Marshal(sendRequest{
		From: msg.From().String(),
		To:   msg.To().String(),
		Body: msg.Body(),
	})
	if err != nil {
		return nil, err
	}

	return http.NewRequestWithContext(ctx, "POST", endpoint.String(), bytes.NewBuffer(reqBytes))
}
