package creating

import (
	"context"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal"
)

// Service defines the expected behaviour from a creating service.
type Service interface {
	// CreateMessage creates a new message in the system.
	CreateMessage(context.Context, string, string, string) error
}

// DefaultService is the default Service interface
// implementation returned by creating.NewNewService().
type DefaultService struct {
	messageRepository comms.MessageRepository
}

// NewService returns the default Service interface implementation.
func NewService(messageRepository comms.MessageRepository) DefaultService {
	return DefaultService{
		messageRepository: messageRepository,
	}
}

// CreateMessage implements the creating.Service interface.
func (s DefaultService) CreateMessage(ctx context.Context, from, to, body string) error {
	message, err := comms.NewMessage(from, to, body)
	if err != nil {
		return err
	}

	return s.messageRepository.Save(ctx, message)
}
