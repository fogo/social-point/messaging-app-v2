package bootstrap

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/cli/cmd"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/cli/cmd/send"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/cli/cmd/version"
	httpsender "gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/http/sender"
)

const (
	appName    = "primcom"
	appVersion = "v0.1.0"

	remoteAddress = "http://localhost:8000"
)

func Run() error {
	httpClient := &http.Client{
		Timeout: 10 * time.Second,
	}

	sendEndpoint, err := url.Parse(fmt.Sprintf("%s/%s", remoteAddress, "messages"))
	if err != nil {
		return err
	}

	var (
		messageSender = httpsender.NewSender(httpClient, sendEndpoint)
	)

	ctx := context.Background()

	rootCmd := cmd.NewClientCmd(ctx)
	rootCmd.AddCommand(send.NewSendCmd(ctx, messageSender))
	rootCmd.AddCommand(version.NewVersionCmd(ctx, appName, appVersion))

	return rootCmd.Execute()
}
