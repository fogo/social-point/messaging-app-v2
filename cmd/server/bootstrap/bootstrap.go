package bootstrap

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/creating"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/http/server/handler/messages"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/http/server/middleware/logging"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/http/server/middleware/recovery"
	"gitlab.com/fogo/social-point/messaging-app-v2/internal/platform/storage/inmemory"
)

const (
	address = ":8000"
)

func Run() error {
	var (
		messageRepository = inmemory.NewMessageRepository()
		creatingService   = creating.NewService(messageRepository)
	)

	return runServer(creatingService)
}

func runServer(
	creatingService creating.Service,
) error {
	engine := gin.New()
	engine.Use(logging.Logging(), recovery.Recovery())
	engine.POST("/messages", messages.CreateMessage(creatingService))

	return engine.Run(address)
}
